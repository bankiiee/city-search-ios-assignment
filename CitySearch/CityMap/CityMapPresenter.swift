//
//  CityMapPresenter.swift
//  CitySearch
//
//  Created by Sakarn Limnitikarn on 10/3/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import MapKit
import UIKit

protocol CityMapPresentationLogic {
    func presentCityMap(response: CityMap.DisplayCity.Response)
}

class CityMapPresenter: CityMapPresentationLogic {
    weak var viewController: CityMapDisplayLogic?

    // MARK: Do something

    func presentCityMap(response: CityMap.DisplayCity.Response) {
        switch response.result {
        case .success(let city):
            viewController?.displayCityMap(
                viewModel: .init(
                    viewModel: .success(
                        .init(
                            title:"\(city.name), \(city.country)",
                            subtitle: "",
                            coordinate: .init(
                                latitude: city.coordinate.latitude,
                                longitude: city.coordinate.longitude
                            )
                        )
                    )
                )
            )
        case .failure(let error):
            viewController?.displayCityMap(viewModel: .init(viewModel: .failure(error)))
        }
    }
}
