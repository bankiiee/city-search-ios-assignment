//
//  CityAnnotation.swift
//  CitySearch
//
//  Created by Sakarn Limnitikarn on 10/3/22.
//

import MapKit
import UIKit

class CityAnnotation: MKPointAnnotation {
    init(
        title: String? = nil,
        subtitle: String? = nil,
        coordinate: CLLocationCoordinate2D)
    {
        super.init()
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
    }
}
