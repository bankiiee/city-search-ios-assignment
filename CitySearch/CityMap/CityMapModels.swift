//
//  CityMapModels.swift
//  CitySearch
//
//  Created by Sakarn Limnitikarn on 10/3/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import MapKit

enum CityMap {
    // MARK: Use cases
    enum CityMapError: Error {
        case noCity
    }
    enum DisplayCity {
        struct Request {}

        struct Response {
            let result: Result<City, CityMapError>
           
        }

        struct ViewModel {
            let viewModel: Result<CityAnnotation, CityMapError>
        }
    }
}
