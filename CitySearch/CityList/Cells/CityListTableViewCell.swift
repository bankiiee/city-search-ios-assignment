//
//  CityListTableViewCell.swift
//  CitySearch
//
//  Created by Sakarn Limnitikarn on 9/3/22.
//

import UIKit

class CityListTableViewCell: UITableViewCell {
    static let identifier = "CityListTableViewCell"
    
    @IBOutlet var titleTextLabel: UILabel!
    @IBOutlet var subtitleTextLabel: UILabel!
    
    func configure(viewModel: CityListTableViewCellViewModel) {
        titleTextLabel.text = viewModel.title
        subtitleTextLabel.text = viewModel.subtitle
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleTextLabel.text = ""
        subtitleTextLabel.text = ""
    }

}
