//
//  CityListTableViewCellViewModel.swift
//  CitySearch
//
//  Created by Sakarn Limnitikarn on 9/3/22.
//

import Foundation

struct CityListTableViewCellViewModel {
    let title: String
    let subtitle: String
    
    init(city: City) {
        title = "\(city.name), \(city.country)"
        subtitle = "lat: \(city.coordinate.latitude), long: \(city.coordinate.longitude)"
    }
}
