//
//  City.swift
//  CitySearch
//
//  Created by Sakarn Limnitikarn on 8/3/22.
//

import Foundation

struct City: Codable {
    struct Coordinate: Codable {
        let latitude: Double
        let longitude: Double
        
        enum CodingKeys: String, CodingKey {
            case latitude = "lat"
            case longitude = "lon"
        }
    }
    
    let id: Int
    let name: String
    let country: String
    let coordinate: Coordinate
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case country
        case coordinate = "coord"
        
    }
}
