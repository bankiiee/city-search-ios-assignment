//
//  DataAssetManager.swift
//  CitySearch
//
//  Created by Sakarn Limnitikarn on 8/3/22.
//

import Foundation
import UIKit

class DataAssetsManager {
    enum DataAssetsManagerError: Error {
        case fileNotFound
    }

    public static let shared = DataAssetsManager()

    private init() {}

    func fetchJSON(from fileName: String, completion: ([City]?) -> Void) {
        let assets = NSDataAsset(name: fileName, bundle: .main)
        guard let jsonData = assets?.data,
              let cities = try? JSONDecoder().decode([City].self, from: jsonData)
        else {
            completion(nil)
            return
        }

        completion(cities)
    }
}
